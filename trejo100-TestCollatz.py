#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "10 1999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1999)

    def test_read_3(self):
        s = "9538 500\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  9538)
        self.assertEqual(j, 500)

    def test_read_4(self):
        s = "333 333\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  333)
        self.assertEqual(j, 333)

    def test_read_5(self):
        s = "1 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 200)


    # ----
    # eval
    # ----


    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(100, 100)
        self.assertEqual(v, 26)

    def test_eval_6(self):
        v = collatz_eval(2, 9)
        self.assertEqual(v, 20)

    def test_eval_7(self):
        v = collatz_eval(7937, 195)
        self.assertEqual(v, 262)

    def test_eval_8(self):
        v = collatz_eval(9999, 60)
        self.assertEqual(v, 262)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 450, 15, 144)
        self.assertEqual(w.getvalue(), "450 15 144\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1, 11, 20)
        self.assertEqual(w.getvalue(), "1 11 20\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 100, 101, 26)
        self.assertEqual(w.getvalue(), "100 101 26\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("9999 60\n888 6999\n465 9763\n3 3333\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "9999 60 262\n888 6999 262\n465 9763 262\n3 3333 217\n")

    def test_solve_3(self):
        r = StringIO("1947 29\n574 9\n4937 2000\n82 1111\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1947 29 182\n574 9 144\n4937 2000 238\n82 1111 179\n")

    def test_solve_4(self):
        r = StringIO("836 6020\n7492 8402\n100 100\n100 101\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "836 6020 238\n7492 8402 252\n100 100 26\n100 101 26\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
